﻿using System.Collections.Generic;
using Jypeli;

/// @author Tuukka Tikanoja
/// @version 5.4.2018
/// <summary>
/// Disgruntlement peli
/// </summary>
public class Disgruntlement : PhysicsGame
{
    private double liikeNopeus = 200;
    private double hyppyKorkeus = 800;

    private int kenttaNro;

    private int[] kuolemat = new int[] { 0, 0, 0 };

    private PlatformCharacter hahmo;

    private PhysicsObject piikki;
    private PhysicsObject maali;
    private PhysicsObject booster;

    private Image[] kavelyAnimaatio = LoadImages("kavely1", "kavely2");
    private Image[] paikallaanAnimaatio = LoadImages("hahmotekstuuri");
    private Image[] putoamisAnimaatio = LoadImages("hyppy2");

    private Image alkuValikko = LoadImage("alkuvalikko");
    private Image hahmoTekstuuri = LoadImage("hahmotekstuuri");
    private Image piikkiTekstuuri = LoadImage("piikkitekstuuri");
    private Image taustakuva = LoadImage("taustakuva");
    private Image lattiaTekstuuri = LoadImage("lattiatekstuuri");
    private Image maaliTekstuuri = LoadImage("maalitekstuuri");
    private Image boosterTekstuuri = LoadImage("boostertekstuuri");
    private Image loppuValikko = LoadImage("loppuvalikko");

    /// <summary>
    /// Aloittaa pelin laittamalla fullscreeniin ja menee alkuvalikkoon
    /// </summary>
    public override void Begin()
    {
        IsFullScreen = true;
        AlkuValikko();
    }


    /// <summary>
    /// Tekee alkuvalikon
    /// </summary>
    private void AlkuValikko()
    {
        ClearAll();

        kuolemat = new int[] { 0, 0, 0 };
        kenttaNro = 1;

        Level.Background.Image = alkuValikko;

        List<Label> valikonKohdat;
        valikonKohdat = new List<Label>();

        Label kohta1 = new Label("Disgruntlement");
        kohta1.Position = new Vector(0, 200);
        kohta1.Font = Font.DefaultLarge;
        kohta1.TextColor = Color.Yellow;
        valikonKohdat.Add(kohta1);

        Label kohta2 = new Label("Aloita uusi peli");
        kohta2.Position = new Vector(0, 40);
        valikonKohdat.Add(kohta2);

        Label kohta3 = new Label("Lopeta peli");
        kohta3.Position = new Vector(0, -40);
        valikonKohdat.Add(kohta3);

        foreach (Label valikonKohta in valikonKohdat)
        {
            Add(valikonKohta);
        }

        Mouse.ListenOn(kohta2, MouseButton.Left, ButtonState.Pressed, KenttaValitsin, "Aloittaa ensimmäisestä kentästä");
        Mouse.ListenOn(kohta3, MouseButton.Left, ButtonState.Pressed, Exit, "Poistuu pelistä");
    }


    /// <summary>
    /// Aloittaa halutun kentän
    /// </summary>
    private void KenttaValitsin()
    {
        ClearAll();

        if (kenttaNro == 1) LisaaKentta("kentta1");
        else if (kenttaNro == 2) LisaaKentta("kentta2");
        else if (kenttaNro == 3) LisaaKentta("kentta3");
        else if (kenttaNro > 3) LoppuValikko();
    }


    /// <summary>
    /// Lisää kentän ja hahmon peliin, sekä lisää ohjauksen, painovoiman,kameran ja hahmo nopeudet
    /// </summary>
    /// <param name="kentta">Lisättävän kentän nimi</param>
    private void LisaaKentta(string kentta)
    {
        ClearAll();
        LisaaHahmo();
        LisaaBooster();

        ColorTileMap ruudut = ColorTileMap.FromLevelAsset(kentta);
        ruudut.SetTileMethod(Color.Black, LuoLattia);
        ruudut.SetTileMethod(Color.Red, LuoMaali);
        ruudut.SetTileMethod(Color.White, LisaaPiikki);
        ruudut.Execute(13, 13);

        Level.CreateHorizontalBorders();
        Level.Background.Image = taustakuva;
        Level.Background.FitToLevel();

        Ohjaus(0);

        Gravity = new Vector(0, -1000);
        liikeNopeus = 200;
        hyppyKorkeus = 800;

        Camera.FollowY(hahmo);
        Camera.StayInLevel = true;
    }


    /// <summary>
    /// Lisää hahmon, hahmoon tekstuurit ja animaatiot ja laittaa oikeaan paikkaan 
    /// </summary>
    private void LisaaHahmo()
    {
        hahmo = new PlatformCharacter(40, 60);

        if (kenttaNro == 1 || kenttaNro == 2) hahmo.Position = new Vector(0, -500);
        else if (kenttaNro == 3) hahmo.Position = new Vector(-200, -1000);

        hahmo.Image = hahmoTekstuuri;

        AddCollisionHandler(hahmo, "piikki", HahmoTormasi);
        AddCollisionHandler(hahmo, "booster", HahmoTormasiBoosteriin);

        hahmo.AnimWalk = new Animation(kavelyAnimaatio);
        hahmo.AnimWalk.FPS = 5;
        hahmo.AnimIdle = new Animation(paikallaanAnimaatio);
        hahmo.AnimFall = new Animation(putoamisAnimaatio);

        Add(hahmo);
    }


    /// <summary>
    /// Kun hahmo osuu piikkeihin, aloittaa kentän alusta ja lisää kuoleman
    /// </summary>
    /// <param name="tormaaja">Olio joka törmää</param>
    /// <param name="kohde">Olio johon hahmo törmäsi</param>
    private void HahmoTormasi(PhysicsObject tormaaja, PhysicsObject kohde)
    {
        kuolemat[kenttaNro-1]++;
        KenttaValitsin();
    }


    /// <summary>
    /// Lisää boosterin kenttään
    /// </summary>
    /// <param name="kentta"></param>
    private void LisaaBooster()
    {
        booster = new PhysicsObject(26, 26);

        if (kenttaNro == 1) booster.Position = new Vector(-253, -338);
        else if (kenttaNro == 2) booster.Position = new Vector(-50, -38);
        else if (kenttaNro == 3) booster.Position = new Vector(200,  -160);

        booster.Tag = "booster";
        booster.MakeStatic();
        booster.Image = boosterTekstuuri;
        Add(booster);
    }


    /// <summary>
    /// Kun hahmo osuu boosteriin niin tekee pelistä hidastetun ja ohjauksesta käänteisen
    /// </summary>
    /// <param name="tormaaja"></param>
    /// <param name="kohde"></param>
    private void HahmoTormasiBoosteriin(PhysicsObject tormaaja, PhysicsObject kohde)
    {
        Gravity = new Vector(0, -300);
        hyppyKorkeus = 295;
        liikeNopeus = 110;

        Ohjaus(1);

        booster.Destroy();
    }


    /// <summary>
    /// Lisää piikit kenttään
    /// </summary>
    /// <param name="paikka">mihin tulee</param>
    /// <param name="leveys">olion leveys</param>
    /// <param name="korkeus">olion korkeus</param>
    private void LisaaPiikki(Vector paikka, double leveys, double korkeus)
    {
        piikki = new PhysicsObject(leveys, korkeus);
        piikki.Position = paikka;
        piikki.CollisionIgnoreGroup = 1;
        piikki.Tag = "piikki";
        piikki.MakeStatic();
        piikki.Image = piikkiTekstuuri;
        Add(piikki);
    }


    /// <summary>
    /// Tekee kenttään lattian
    /// </summary>
    /// <param name="paikka">Tason paikka</param>
    /// <param name="leveys">Tason leveys</param>
    /// <param name="korkeus">Tason korkeus</param>
    private void LuoLattia(Vector paikka, double leveys, double korkeus)
    {
        PhysicsObject taso = PhysicsObject.CreateStaticObject(leveys, korkeus);
        taso.Position = paikka;
        taso.CollisionIgnoreGroup = 1;
        taso.Image = lattiaTekstuuri;
        Add(taso);
    }


    /// <summary>
    /// lisää maalin kenttään
    /// </summary>
    /// <param name="paikka">mihin tulee</param>
    /// <param name="leveys">olion leveys</param>
    /// <param name="korkeus">olion korkeus</param>
    private void LuoMaali(Vector paikka, double leveys, double korkeus)
    {
        maali = new PhysicsObject(40, 100);
        maali.Position = new Vector(paikka.X, paikka.Y - 9.7);
        maali.MakeStatic();
        AddCollisionHandler(hahmo, maali, HahmoMeniMaaliin);
        maali.Image = maaliTekstuuri;
        Add(maali);
    }


    /// <summary>
    /// Vaihtaa kenttää kun hahmo osuu maaliin
    /// </summary>
    /// <param name="tormaaja">Törmääjä</param>
    /// <param name="kohde">Kohde</param>
    private void HahmoMeniMaaliin(PhysicsObject tormaaja, PhysicsObject kohde)
    {
        kenttaNro++;
        KenttaValitsin();
    }


    /// <summary>
    /// Lisää ohjauksen peliin
    /// </summary>
    /// <param name="kaanteiset">Onko ohjaus käänteistä</param>
    private void Ohjaus(int kaanteiset)
    {
        ClearControls();
        if (kaanteiset == 0)
        {
            Keyboard.Listen(Key.Right, ButtonState.Down, Liiku, "Liikkuu oikealle", 0);
            Keyboard.Listen(Key.Left, ButtonState.Down, Liiku, "Liikkuu vasemmalle", 1);
        }
        else
        {
            Keyboard.Listen(Key.Right, ButtonState.Down, Liiku, "Liikkuu oikealle", 1);
            Keyboard.Listen(Key.Left, ButtonState.Down, Liiku, "Liikkuu vasemmalle", 0);
        }
        Keyboard.Listen(Key.Up, ButtonState.Down, Hyppaa, "Liikkuu ylöspäin");
        Keyboard.Listen(Key.Escape, ButtonState.Pressed, AlkuValikko, "Valikkoon");
        Keyboard.Listen(Key.R, ButtonState.Pressed, KenttaValitsin, "Aloittaa kentän alusta");
    }


    /// <summary>
    /// Liikuttaa hahmoa
    /// </summary>
    /// <param name="liikeNopeus">Liikkeen nopeus</param>
    private void Liiku(int suunta)
    {
        if (suunta == 0) hahmo.Walk(liikeNopeus);
        else hahmo.Walk(-liikeNopeus);
    }


    /// <summary>
    /// Pistää hahmon hyppäämään
    /// </summary>
    private void Hyppaa()
    {
        hahmo.Jump(hyppyKorkeus);
    }


    /// <summary>
    /// Lisätään loppuvalikko
    /// </summary>
    private void LoppuValikko()
    {
        ClearAll();

        kenttaNro = 1;

        Level.Background.Image = loppuValikko;

        List<Label> valikonKohdat;
        valikonKohdat = new List<Label>();

        Label kohta1 = new Label("Onneksi olkoon läpäisit pelin!");
        kohta1.Position = new Vector(0, 200);
        kohta1.Font = Font.DefaultLarge;
        kohta1.TextColor = Color.Yellow;
        valikonKohdat.Add(kohta1);

        Label kohta2 = new Label("Aloita uudelleen");
        kohta2.Position = new Vector(0, 40);
        kohta2.TextColor = Color.White;
        valikonKohdat.Add(kohta2);

        Label kohta3 = new Label("Lopeta peli");
        kohta3.Position = new Vector(0, -40);
        kohta3.TextColor = Color.White;
        valikonKohdat.Add(kohta3);

        Label kohta4 = new Label("Kuolemia: " + KuolemienMaara(4));
        kohta4.Position = new Vector(0, 100);
        kohta4.TextColor = Color.Red;
        valikonKohdat.Add(kohta4);

        foreach (Label valikonKohta in valikonKohdat)
        {
            Add(valikonKohta);
        }

        Mouse.ListenOn(kohta2, MouseButton.Left, ButtonState.Pressed, KenttaValitsin, "Aloittaa ensimmäisestä kentästä");
        Mouse.ListenOn(kohta3, MouseButton.Left, ButtonState.Pressed, Exit, "Poistuu pelistä");
    }


    /// <summary>
    /// laskee kuolemien määrän
    /// </summary>
    /// <param name="kentta">mistä kentästä kuolemat haluaa</param>
    /// <returns>kuolemien määrän halutusta kentästä tai kaikista yhteensä</returns>
    private int KuolemienMaara(int kentta)
    {
        if (kentta == 4) return kuolemat[0] + kuolemat[1] + kuolemat[2];
        else return kuolemat[kentta-1];
    }
}